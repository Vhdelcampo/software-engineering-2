// To save as "<TOMCAT_HOME>\webapps\hello\WEB-INF\classes\HelloServlet.java"
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
 
public class HelloServlet extends HttpServlet {
   @Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
 
      // Set the response MIME type of the response message
      response.setContentType("text/html");
      // Allocate a output writer to write the response message into the network socket
      PrintWriter out = response.getWriter();
 
      // Write the response message, in an HTML page
      try {
	  out.println("
	  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		      <thead>
		      <tr>
		      <th>Title</th>
		      <th>Type</th>
		      <th>Role</th>
		      <th>Last Modified Date</th>
		      </tr>
		      </thead>
		      <tbody>
	
		      <tr>
		      <td><a href="activity.html">Activity Title 1</a></td>
		      <td>Activity</td>
		      <td>Owner</td>
		      <td class="center">MM-DD-YY</td>
		      </tr>

		      <tr>
		      <td><ul class="metismenu" id="menu">
		      <li class="active">
		      <a href="#" aria-expanded="true">Initiative Title 1</a>
		      <ul aria-expanded="true">
		      Activity Title 1
		      </ul>
		      </li>
		      <li>
		      <a href="#" aria-expanded="false">Initiative Title 2</a>
		      <ul aria-expanded="false">
		      Activity Title 2
		      </ul>
		      </li>
		      </ul>
		      </td>
		      </tr>
	
		      <tr>
		      <td><a href="initiative.html">Initiative Title 3</a></td>
		      <td>Initaitive</td>
		      <td>Initiative Administrator</td>
		      <td class="center">MM-DD-YY</td>
		      </tr>
		      </tbody>
		      </table>"
		      );
         out.println("<html>");
         out.println("<head><title>Hello, World</title></head>");
         out.println("<body>");
         out.println("<h1>Hello, world!</h1>");  // says Hello
         // Echo client's request information
         out.println("<p>Request URI: " + request.getRequestURI() + "</p>");
         out.println("<p>Protocol: " + request.getProtocol() + "</p>");
         out.println("<p>PathInfo: " + request.getPathInfo() + "</p>");
         out.println("<p>Remote Address: " + request.getRemoteAddr() + "</p>");
         // Generate a random number upon each request
         out.println("<p>A Random Number: <strong>" + Math.random() + "</strong></p>");
         out.println("</body></html>");
      } finally {
         out.close();  // Always close the output writer
      }
   }
}
